package com.cg.insert.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.cg.insert.dto.customer;


@Repository
public interface insert extends MongoRepository<customer, Integer>  {
}