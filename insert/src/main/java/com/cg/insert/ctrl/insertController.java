package com.cg.insert.ctrl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cg.insert.dto.customer;
import com.cg.insert.repository.insert;

@RestController
public class insertController {
	
	@Autowired
	insert repo;
	
	@PostMapping("/customers/insert/")
	public customer insertCust(@RequestBody customer cust)	{
		repo.save(cust);
		return cust;
	}
}
