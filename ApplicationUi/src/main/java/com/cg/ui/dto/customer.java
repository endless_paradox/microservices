package com.cg.ui.dto;

import org.springframework.stereotype.Component;

@Component
public class customer {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public customer(int id, String name) {
        super();
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "customer [id=" + id + ", name=" + name + "]";
    }

    public customer() {
        super();
    }
}
