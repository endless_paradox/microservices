package com.cg.ui.ctrl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.cg.ui.dto.customer;

@RestController
public class uiController {

	@Autowired
	RestTemplate temp;
	
	@GetMapping("/customers/")
	public customer[] getAll()	{
		return temp.getForObject("http://get-data/customer", customer[].class);
	}
	
	@GetMapping("/customers/{id}")
	public customer getById(@PathVariable("id") int id)	{
		return temp.getForObject("http://get-data/customer/"+id, customer.class);
	}
	
	@PostMapping("/customer/insert/")
	public customer insertCust(@RequestBody customer cust)	{
		customer cust1 = temp.postForObject("http://insert-data/customers/insert/", cust, customer.class);
		return cust1;
	}
	
	@PutMapping("/customer/update/{id}")
	public @ResponseBody customer updateCust(@RequestBody customer cust, @PathVariable("id") int id)	{
		temp.put("http://update-data/customer/update/"+id, cust);
		customer cust1 = temp.getForObject("http://get-data/customer/"+id, customer.class);
		return cust1;
	}
	@DeleteMapping("/customer/delete/{id}")
	public customer deleteCust(@PathVariable("id") int id)	{
		customer cust1 = temp.getForObject("http://get-data/customer/"+id, customer.class);
		temp.delete("http://delete-data/customer/delete/"+id);
		return cust1;
	}
}
