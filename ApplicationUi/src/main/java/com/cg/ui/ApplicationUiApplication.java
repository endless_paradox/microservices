package com.cg.ui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableEurekaClient
@ComponentScan(basePackages = "com.cg.ui")
public class ApplicationUiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApplicationUiApplication.class, args);
	}
	@LoadBalanced
	@Bean
	public RestTemplate getRestTemplate()	{
		return new RestTemplate();
	}
}
