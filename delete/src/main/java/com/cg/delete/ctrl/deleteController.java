package com.cg.delete.ctrl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.cg.delete.dto.customer;
import com.cg.delete.repository.deleteRepo;

@RestController
public class deleteController {

	@Autowired
	deleteRepo repo;
	
	@DeleteMapping("/customer/delete/{id}")
	public customer deleteCust(@PathVariable("id") int id)	{
		customer cust = repo.findById(id).get();
		repo.delete(cust);
		return cust;
	}
}
