package com.cg.delete.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.cg.delete.dto.customer;

@Repository
public interface deleteRepo extends MongoRepository<customer, Integer>{

}
