package com.cg.update.ctrl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cg.update.dto.customer;
import com.cg.update.repository.updateRepo;

@RestController
public class updateController {

	@Autowired
	updateRepo repo;
	
	@PutMapping("/customer/update/{id}")
	public customer updateCust(@RequestBody customer cust, @PathVariable("id") int id)	{
		cust.setId(id);
		repo.save(cust);
		return cust;
	}
}
