package com.cg.update.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.cg.update.dto.customer;

@Repository
public interface updateRepo extends MongoRepository<customer, Integer> {

}
