package com.cg.get;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableEurekaClient
@ComponentScan(basePackages = "com.cg.get")
public class RetreieveApplication {

	public static void main(String[] args) {
		SpringApplication.run(RetreieveApplication.class, args);
	}

}
