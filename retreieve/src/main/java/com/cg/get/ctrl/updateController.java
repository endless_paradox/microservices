package com.cg.get.ctrl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.cg.get.dto.customer;
import com.cg.get.repository.getRepo;

@RestController
public class updateController {
	
	@Autowired
	getRepo repo;
	
	@GetMapping("/customer")
	public Iterable<customer> getAll()	{
		return repo.findAll();
	}
	
	@GetMapping("/customer/{id}")
	public customer getById(@PathVariable("id") int id)	{
		return repo.findById(id).get();
	}
}
