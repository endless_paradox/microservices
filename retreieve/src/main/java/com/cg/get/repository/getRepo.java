package com.cg.get.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.cg.get.dto.customer;

@Repository
public interface getRepo extends MongoRepository<customer, Integer>	{

}
