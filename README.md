# Micro Services

Small Application Which Perform CRUD Operations on Database. It divides the CRUD Operations in 4 different Microservices and Uses Eureka Server to connect with each Other and It has a UI which communicates with the Micro Services.

### Technology Used

- Eureka
- Spring Boot
- Maven
- Java 1.8

### Software Used

- MongoDB
- VS Code

### Operating System

- ARCH Linux
